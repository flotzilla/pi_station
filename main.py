import sys
import time
import Adafruit_BMP.BMP085 as BMP085

from luma.core.interface.serial import spi
from luma.core.render import canvas
from luma.oled.device import ssd1351
from datetime import datetime
from PIL import ImageFont
from entities.OpenWeatherMap import Owm

fnt30 = ImageFont.truetype('fonts/OpenSans-Regular.ttf', 30)
fnt15 = ImageFont.truetype('fonts/OpenSans-Regular.ttf', 15)
fnt10 = ImageFont.truetype('fonts/OpenSans-Regular.ttf', 10)
api_key = '4fd151fccf0e90caa48695350676eaa2'
sleep_time = 1


class Main:
    serial = spi(device=0, port=0)
    device = ssd1351(serial)
    sensor = BMP085.BMP085()

    default_place = 'Kyiv'
    
    w = None
    c = 0
    owm = Owm(api_key, default_place)

    def __init__(self):
        self.every_min()

    def run(self):
        with canvas(self.device) as draw:
            draw.rectangle(self.device.bounding_box, outline="white", fill="black")
            draw.text((30, 40), "Hello there", fill="green")

        try:
            while True:
                ctime = datetime.now()
                temp = self.sensor.read_temperature()
                pressure = self.sensor.read_pressure()

                with canvas(self.device) as draw:
                    draw.text((5, 40), ctime.strftime('%H:%M:%S'), fill="green", font=fnt30)
                    draw.text((25, 80), ctime.strftime('%Y-%m-%d'), fill="orange", font=fnt15)
                    draw.text((15, 100), '{0:0.1f} C'.format(temp), font=fnt10)
                    draw.text((60, 100), '{0:0.2f} Pa'.format(pressure * 0.00750062), font=fnt10)

                    if self.w is not None and self.w is not 'error' and self.w.has_no_errors():
                        draw.text((5, 5), self.default_place, fill="white", font=fnt10)
                        draw.text((5, 20), self.w.get_temp() + 'C' , fill="green", font=fnt10)
                        draw.text((50, 20), self.w.get_humidity(), fill="green", font=fnt10)
                        draw.text((80, 20), self.w.get_pressure(), fill="green", font=fnt10)

                time.sleep(sleep_time)

                if self.c == 60:
                    self.every_min()
                    self.c = 0
                else:
                    self.c = self.c + 1

        except Exception as e:
            print(e)

        except KeyboardInterrupt as ke:
            print('\r\n', 'done')
            sys.exit(1)

    def every_min(self):
        try:
            self.w = self.owm.get_weather()
        except Exception as e:
            self.w = None
            print(e)


if __name__ == "__main__":
    m = Main()
    m.run()
