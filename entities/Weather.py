__author__ = '0byte'


class Weather:
    # string that contains json answer
    weather = None

    def __init__(self, weather):
        self.weather = weather

    def has_no_errors(self):
        is_true = True
        if self.weather['cod'] is not None:
            if self.weather['cod'] == '404':
                is_true = False
        return is_true

    def get_temp(self):
        t = self.weather['main']['temp']
        return str(t)

    def get_humidity(self):
        return str(self.weather['main']['humidity']) + '%'

    def get_weather(self):
        w = self.weather['weather'][0]
        return w['main'] + ' ' + w['description']

    def get_pressure(self):
        w = self.weather['main']['pressure']
        return str(w) + ' hPa'
